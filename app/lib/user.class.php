<?php
/**
 * Class User  
 *
 *
 * @author Mohamed Maail <mohamed.maail@gmail.com>
 */

require 'ldap.class.php';

class User {
	// verifies the user with the provided _POST username & password
	public static function verifyUser($username, $password)
	{		
	    $ldap = new adLDAP();
		$ldap->authenticate($username,$password);
		echo $ldap->get_last_error();
	}
}
?>