<?php

// index page - shows the login page
// TODO - LDAP integration
$app->get("/", function() use($app) {
	$app->render("login/login_form.html");
	$app->flashNow('error', 'Fill in username and password first');
});

// validation against the HR system through LDAP
$app->post("/validate", function() use ($app){
	// get request body, decode into php object
	$request = $app->request();

	// assign _POST to vars
	$username = $request->post("username");
	$password = $request->post("password");

	// verify the _POST username and password
	if (!empty($username) && !empty($password)) {
		User::verifyUser($username, $password);
	} else {
		echo "Username and password cannot be blank";
	}
	
});

// get method for validation against the get path of the above post path
$app->get("/validate", function(){
	echo "<a href='/adapi'>&larr; Go back to login</a>. You can't access post pages just like that. ";
});

// forgot password page
// is this necessary?
$app->get("/forgotpw", function() use($app) {
	// print something as for now
	echo "This is the forgot password page";
});

?>