<?php

// @author Maxsy
// @email  maxii.zex@gmail.com

// require the application libraries
require '../Slim/Slim.php';
require '../app/lib/user.class.php';
require '../Mustache/mustacheview.php';

//  registers the autoloader
\Slim\Slim::registerAutoloader();

// slim config array
$app = new \Slim\Slim(array(
    "mode" => "development",
    'view'          => new View_Mustache(),
    "templates.path" => "../app/views/",
));


// require the routes files
// resort to a loop as application gets bigger
require '../app/routes/authentication.php';

// run the application
$app->run();

?>